/* Just make this easier for you
 * Here are all the ID I created:
 * button bar at top:
 *     (button name)           ID
 *        Cars         ->   cars
 *        History      ->   history
 *        Today's Job  ->   update
 *
 *     (table name)
 *   table for cars       ->  car-table
 *   table for history    ->  history-table
 *   table for today job  ->  update-table
 *
 *     (Screen name)
 *       Cars             ->  cars
 *   transaction History  ->  history
 *    today's job         ->  update
 *
 *   (Update popup window)
 *   popup window      ->   UpdatePopUp
 *   update form       ->   UpdateForm
 *   (input bar)
 *   first-name         ->   first-name
 *   last-name          ->   last-name
 *   customer #        ->   unique-number
 *   rental time       ->   rental-time
 *   car #             ->   car-number
 *   rental balance    ->   rental-balance
 *   update user button ->  UpdateUser
 *
 */





var UserDataBase = new Array();
var logged = false;
var username;
var Employeelastname;
var password;
var eid = 0;
// Login
// Those chunk of codes are for log in screen, just did not finish yet.

function ShowScreen(id){
	document.getElementById(id).style.display="block";
}

function Login(){
        ToggleLoginStatus();
        document.getElementById("login-screen-2").style.display="none";
        document.getElementById("login-screen").style.display="none";
        SwitchScreen(event, "today");
}

function LoginCancel(id){
        document.getElementById(id).style.display="none";
}

function LogOut(){
    document.getElementById("login-screen").style.display = "block";
    document.getElementById("account-change-form").reset();
}

function ToggleLoginStatus() {
    logged = !logged;
    if (logged === false) {
        username = null;
        password = null;
        uid = null;
    }
}

function ResetInputs() {
    var elements = document.getElementsByTagName("form");
    for (var i = 0; i < elements.length; i++) {
        elements[i].reset();
    }
}
function CheckLogin(formname) {
    username = document.getElementById(formname).elements[0].value;
    var transfername = username.toUpperCase();
    ChangeAccountInfo();
    switch(transfername) {
        case 'MARK':
            username = '1';
            break;
        case 'DAIVD':
            username = '2';
            break;
        case 'KEVIN':
            username = '3';
            break;
        case 'RICH':
            username = '4';
            break;
    }
    password = document.getElementById(formname).elements[1].value;
    checkEmployeeLogin(username, password, LoginCheck);
    GetEmployee();
}

var employeeFile;
function GetEmployee() {
    var data = new FormData();
    data.append("action", "employees");
    outPost(data, "http://4dudes.servebeer.com/backend/handler.py", function (cbt) {
        obj = JSON.parse(cbt);
        for (var i in obj) {
            if (obj[i]["EID"] === parseInt(username)) {
                employeeFile = obj[i];
            }
        }
    });
}

function ChangeToSetting() {
    document.getElementById("account-change-form").elements[0].value = employeeFile["FirstName"];
    document.getElementById("account-change-form").elements[1].value = employeeFile["LastName"];
    document.getElementById("account-change-form").elements[2].value = employeeFile["Password"];
}
function LoginCheck(ctb) {
    if (ctb > 0) {
        Login();
        document.getElementById("login-form").elements[0].style.border="1px solid buttonface";
        document.getElementById("login-form").elements[1].style.border="1px solid buttonface";
        document.getElementById("login-input-wrong").innerHTML = "";
    }
    else {
        document.getElementById("login-form").elements[0].style.border="2px solid red";
        document.getElementById("login-form").elements[1].style.border="2px solid red";
        document.getElementById("login-input-wrong").innerHTML = "Invalid email or password";
    }
}
function ChangeAccountInfo() {
    var data = new FormData();
    data.append('action', 'employees');
    //data.append('eid', 1),
    outPost(data, "http://4dudes.servebeer.com/backend/handler.py", SaveAccountInfo);
    
}
function SaveAccountInfo(string) {
    var isManager;
    var obj = JSON.parse(string);
	for (var i in obj) {
		if (obj[i]['FirstName'].toUpperCase() === username.toUpperCase()){
                    eid = obj[i]['EID'];
                    isManager = obj[i]['IsManager'];
                    Employeelastname = obj[i]['LastName'];
                };
	}
}

function CheckChange(ctb) {
    
}

function UpdateEmployeeSetting() {
    var firstname = document.getElementById("account-change-firstname").value;
    var lastname = document.getElementById("account-change-lastname").value;
    var pass = document.getElementById("account-change-password").value;
    updateEmployee(eid, firstname, lastname, 0, pass, CheckEmployeeSetting);
}

function CheckEmployeeSetting(ctb) {
    if (ctb === 1) {
        alert("Account Information has been successfull updated!");
    }
    else {
        alert("Failed to update Account information, please Contact Managers");
    }
}
// Current Leases
// ---------------------------------------------------------------------------------------------
var MonthEnum = {
	JANUARY: "Jan.", FEBRUARY: "Feb.", MARCH: "Mar.", APRIL: "Apr.", MAY: "May.", JUNE: "Jun.",
	JULY: "Jul.", AUGUST: "Aug.", SEPTEMBER: "Sep.", OCTOBER: "Oct.", NOVEMBER: "Nov.", DECEMBER: "Dec."
};
var currentDate = new Date();

// Given a date in DD/MM/YYYY, returns a date in abbreviated month and date format; does not include year
// E.g. date = "31/12/2017" returns "Dec. 31"
function ConvertDates(date) {
	var split = date.split("/");
	var converted = "";
	//day
	converted += split[0]
	//month
	if (split[1] === "1") { converted = MonthEnum.JANUARY + " " + converted; }
	else if (split[1] === "2") { converted = MonthEnum.FEBRUARY + " " + converted; }
	else if (split[1] === "3") { converted = MonthEnum.MARCH + " " + converted; }
	else if (split[1] === "4") { converted = MonthEnum.APRIL + " " + converted; }
	else if (split[1] === "5") { converted = MonthEnum.MAY + " " + converted; }
	else if (split[1] === "6") { converted = MonthEnum.JUNE + " " + converted; }
	else if (split[1] === "7") { converted = MonthEnum.JULY + " " + converted; }
	else if (split[1] === "8") { converted = MonthEnum.AUGUST + " " + converted; }
	else if (split[1] === "9") { converted = MonthEnum.SEPTEMBER + " " + converted; }
	else if (split[1] === "10") { converted = MonthEnum.OCTOBER + " " + converted; }
	else if (split[1] === "11") { converted = MonthEnum.NOVEMBER + " " + converted; }
	else if (split[1] === "12") { converted = MonthEnum.DECEMBER + " " + converted; }
	//year
	converted += ", " + split[2];

	return converted;
}

// Given a date in abbreviated month and date format, returns a date in DD/MM/YYYY
// E.g. date = "Dec. 31, 2017" returns "31/12/2017"
function ConvertDateBack(date) {
	var split = date.split(" ")
	var converted = "";
	//day
	converted += split[1].replace(',','') + "/";
	//month
	if (split[0] === MonthEnum.JANUARY) { converted += "1"; }
	else if (split[0] === MonthEnum.FEBRUARY) { converted += "2"; }
	else if (split[0] === MonthEnum.MARCH) { converted += "3"; }
	else if (split[0] === MonthEnum.APRIL) { converted += "4"; }
	else if (split[0] === MonthEnum.MAY) { converted += "5"; }
	else if (split[0] === MonthEnum.JUNE) { converted += "6"; }
	else if (split[0] === MonthEnum.JULY) { converted += "7"; }
	else if (split[0] === MonthEnum.AUGUST) { converted += "8"; }
	else if (split[0] === MonthEnum.SEPTEMBER) { converted += "9"; }
	else if (split[0] === MonthEnum.OCTOBER) { converted += "10"; }
	else if (split[0] === MonthEnum.NOVEMBER) { converted += "11"; }
	else if (split[0] === MonthEnum.DECEMBER) { converted += "12"; }
	//year
	converted += "/" + split[2];
	
	return converted;
}

// Sets today's date in the custom-date element
function SetTodaysDate() {
	var month = (currentDate.getMonth() + 1).toString();
	var date = currentDate.getDate().toString();
	//console.log(month, date);
	if (month.length === 1) {
		month = "0" + month;
	}
	if (date.length === 1) {
		date = "0" + date;
	}
	document.getElementById("custom-date").value = currentDate.getFullYear() + "-" + month + "-" + date;
}

// Sets the "Current Leases" table
// Value of the date input is returned in the format "YYYY-MM-DD"
function SetTodayTable() {
	var historyTable = document.getElementById("history-table");
	var todayTable = document.getElementById("today-table");
	
	var fullDate = document.getElementById("custom-date").value.split("-");
	var year = fullDate[0];
    var month = fullDate[1].toString(); //keep trailing zeroes
	var date = fullDate[2].toString();
	var fullVal = year + month + date;

	for (var i = 1; i < historyTable.rows.length; i++) {
		var split = historyTable.rows[i].cells[3].innerHTML.split(" - ");
		var rentalTimeStart = ConvertDateBack(split[0]);
		var rtsSplit = rentalTimeStart.split("/");
		if (rtsSplit[1].length === 1) { rtsSplit[1] = "0" + rtsSplit[1]}
		if (rtsSplit[0].length === 1) { rtsSplit[0] = "0" + rtsSplit[0]}

		var rentalTimeEnd = ConvertDateBack(split[1]);
		var rteSplit = rentalTimeEnd.split("/");
		if (rteSplit[1].length === 1) { rteSplit[1]= "0" +rteSplit[1]}
		if (rteSplit[0].length === 1) { rteSplit[0] = "0" + rteSplit[0]}

		var startVal = rtsSplit[2] + rtsSplit[1] + rtsSplit[0];
		var endVal = rteSplit[2] + rteSplit[1] + rteSplit[0];
		//console.log(startVal, endVal, fullVal);
		if (fullVal >= startVal && fullVal <= endVal) {
			var copy = historyTable.rows[i].cloneNode(true);
			todayTable.appendChild(copy);
			for (var j = 1; j < todayTable.rows.length; j++) {
				Cancer(todayTable, j, "today-table", "yellow");
			}(j)
		}(i)
	}
}

// Clears selected table
function ClearTable(tableName) {
	var table = document.getElementById(tableName);
	
	for (var i = table.rows.length-1; i > 0; i--) {
		table.deleteRow(i);
	}(i)
}

// Refreshes "Current Leases" table
function RefreshTodayTable() {
	ClearTable("today-table");
	SetTodayTable();
}
// "Transaction"
// ---------------------------------------------------------------------------------------------
var dCar = null;
var dCust = null;
var dTran = null;
var isupdate =  false;

function AddDefaultExisting() {
	document.getElementById("window-default-new").style.display = "none";
	document.getElementById("window-default-xst").style.display = "block";
}

// 1/3 screen of adding new customer
function AddDefaultNew() {
	document.getElementById("window-default-xst").style.display = "none";
	document.getElementById("window-default-options").style.display = "none";
	document.getElementById("window-default-new").style.display = "block";
	document.getElementById("add-default-form").style.display = "block";
}

// 2/3 screen of adding new customer
function DefaultFormCust() {
	var data = ParseDefaultCust();
	dCust = data;
	AddCustomerRow(data);
	document.getElementById("add-default-form").style.display = "none";
	document.getElementById("default-form").reset();
	document.getElementById("add-default-form2").style.display = "block";
	document.getElementById("window-default-new").style.width = "700px";
	CloneCarTable();
	RefreshCustomerTable;
}

// 3/3 screen of adding new costumer
function DefaultFormCar() {
	dCar = ParseDefaultCar();
	document.getElementById("add-default-form2").style.display = "none";
	document.getElementById("add-default-form3").style.display = "block";
	document.getElementById("window-default-new").style.width = "300px";
}
var recentCID = 0;
// Parse and add to "Transaction" table
function DefaultFormTrans() {
	var data = ParseDefaultTrans();
	dTran = data;
	
	var balance = parseFloat(dTran["AmountDue"]) - parseFloat(dTran["AmountPaid"]); // better import a BigDecimal library
	var rentalTime = ConvertDates(dTran["StartDate"]) + " - " + ConvertDates(dTran["EndDate"]);
	var fullName = dCust["FirstName"] + " " + dCust["LastName"]
	var newCustomer = { 'FirstName': dCust["FirstName"], 'LastName': dCust["LastName"], 'VIN': dCar["VIN"], 'RentalTime': rentalTime, 'BodyStyle': dCar["BodyStyle"], 'Balance': balance }
	AddHistoryRow(newCustomer);
	recentCID = 0;
	RefreshCustomerTable();
	var postData = { "customer": (recentCID + 1), "car": dCar["VIN"], "rentedfrom": dCar["AtBranch"], "startdate": dTran["StartDate"], "enddate": dTran["EndDate"], "amountdue": balance, "rentalby": "Jeb" }
	PushData(postData, "transactions");

	document.getElementById("add-default-form3").style.display = "none";
	document.getElementById("default-form3").reset();
	document.getElementById("add-window-default").click();
	document.getElementById("window-default-options").style.display = "block";
	
	dCar = null;
	dCust = null;
	dTran = null;
}

// Parse default form for customer information
function ParseDefaultCust() {
	var fn = document.getElementById("d-first-name").value;
	var ln = document.getElementById("d-last-name").value;
	var phone = document.getElementById("d-phone").value;
	var email = document.getElementById("d-email").value;
	//parseInt(document.getElementById("customers-table").rows.length);

	var customer = {"FirstName" : fn, "LastName" : ln, "PhoneNumber" : phone, "Email" : email};
	var postData = { "firstname": fn, "lastname": ln, "phonenumber": phone, "email": email, "password": "password" };
	PushData(postData, 'customers');
	return customer;
}

// Parse default form for car information
function ParseDefaultCar() {
	var table = document.getElementById("default-table");
	var temp = [];
	for(var i = 1; i < table.rows.length; i++){
		if(table.rows[i].style.background === "yellow"){
			for (var j = 0; j < table.rows[0].cells.length; j++){
				temp.push(table.rows[i].cells[j].innerHTML);
			}
			//document.getElementById("cars-table").deleteRow(i); //uncomment if we're removing cars
		}
	}
	var car = {"Model" : temp[2], "Colour" : temp[4], "VIN" : temp[0], "Class" : temp[5], "Make" : temp[1], "AtBranch" : temp[6], "BodyStyle" : temp[3]};
	var postData = { "model": temp[2], "colour": temp[4], "vin": temp[0], "class": temp[5], "make": temp[1], "atbranch": temp[6], "bodystyle": temp[3] };
	PushData(postData, 'cars');
	return car;
}

// Parse default form for transaction information
function ParseDefaultTrans() {
	var paid = document.getElementById("d-paid").value;
	var due = document.getElementById("d-due").value;
	var start = document.getElementById("d-start").value;
	var end = document.getElementById("d-end").value;	
	var state = document.getElementById("d-status").value;
	
	var transaction = {"EndDate" : end, "Customer" : dCust['CID'], "RentedFrom" : dCar['AtBranch'], "StartDate" : start, "AmountPaid" : paid, "Status" : state, "AmountDue" : due, "Car" : dCar['VIN'], "RentalBy": null, "ReturnedTo": null, "ReturnedBy": null}
	//var transaction = {'FirstName' : dCust['FirstName'], 'LastName' : dCust['LastName'], 'Car' : dCar['VIN']};
	//push to server
	return transaction;
}

// Abstracted function so ClickOnRow is assigned properly when passed into a for loop
function Cancer(java, script, table, color) {
	java.rows[script].onclick = function () { ClickOnRow(java.rows[script], table, color);};
}

// Clones car table for selection in default form
function CloneCarTable() {
	var src = document.getElementById("cars-table");
	var dst = document.getElementById("default-table");
	var copy = src.cloneNode(true);
	copy.id = "default-table";
	dst.parentNode.replaceChild(copy, dst);
	dst = (document.getElementById("default-table"));
	for (var i = 0; i < dst.rows.length; i++) {
		Cancer(dst, i, "default-table", "yellow");
	}
}
// ---------------------------------------------------------------------------------------------
var currentScreen = 'cars';

// Switches screen
function SwitchScreen(evt, screenName) {
	currentScreen = screenName;
	var screens = document.getElementsByClassName("employee-screen");
	var current = document.getElementsByClassName("tab");
	var buttons = document.getElementsByClassName("side-button");
	for (var i = 0; i < screens.length; i++) {
		screens[i].style.display = "none";
		current[i].className = current[i].className.replace(" w3-dark-gray", "");
	}
	document.getElementById(currentScreen).style.display = "block";
	evt.currentTarget.className += " w3-dark-gray";
	for (var i = 0; i < buttons.length; i++) {
	    buttons[i].style.display = "block";
	}
	if (currentScreen === 'cars') {
	    buttons[2].style.display = "none";
	}
	if (currentScreen === 'today') {
	    buttons[1].style.display = "none";
	    buttons[2].style.display = "none";
	}
}

// Changes clicked on rows to white or yellow
function ClickOnRow(row, table, color) {
    var oldColor = "white";
    ClearSelected(table, oldColor);
    row.style.background = color;
}

// Clears selections for a table
function ClearSelected(table, color) {
    t = document.getElementById(table);
    for (var i = 1; i < t.rows.length; i++) {
        t.rows[i].style.background = color;
    }
}

// Delete a row depending on current tab
function DeleteRow(){
    var currentTable;
    var remote;
	switch(currentScreen){
		case 'cars':
		    currentTable = document.getElementById("cars-table");
		    remote = 'Cars';
			break;
		case 'customers':
		    currentTable = document.getElementById("customers-table");
		    remote = 'Customers';
			break;
		case 'today':
		    currentTable = document.getElementById("today-table");
            remote = 'Transactions'
			break;
		case 'history':
		    currentTable = document.getElementById("history-table");
            remote = 'Transactions'
			break;
	}
	for(var i = currentTable.rows.length -1; i >= 0; i--){
	    if (currentTable.rows[i].style.background === "yellow") {
	        DeleteData(currentTable.rows[i].cells[0].innerHTML, remote);
	        currentTable.deleteRow(i);
	        if (currentScreen === 'history' || currentScreen === 'today') {
	            RefreshHistoryTable();
	            RefreshTodayTable();
	            return;
	        }
	        else { return;}
		}
	}
	alert("Please select a row");
}

function DeleteData(id, remote) {
    var post = new FormData();
    post.append('action', remote);
    post.append('id', id);

    outPost(post, "http://4dudes.servebeer.com/backend/deletehandler.py", function (cbt) { console.log(cbt); })
}

// ---------------------------------------------------------------------------------------------
var currentScreen = 'cars';

document.getElementById('search-box').onkeypress = function(e) {
    var event = e || window.event;
    var charCode = event.which || event.keyCode;

    if ( charCode == '13' ) {
      // Enter pressed
      Search();
      return false;
    }
}

function Search(){
	var currentTable;
	var keyword = document.getElementById("search-box").value.toLowerCase();
	console.log(keyword);
	console.log(currentScreen);
	switch(currentScreen){
		case 'cars':
			currentTable = document.getElementById("cars-table");
			tr = currentTable.getElementsByTagName("tr");
			for (i = 1; i < tr.length; i++) {
				var td1 = tr[i].getElementsByTagName("td")[0];
				var td2 = tr[i].getElementsByTagName("td")[1];
				var vin = "";
				var make = "";

				// Search for VIN.
				if(td1) {
					vin = td1.innerHTML.toLowerCase();
				}

				// Search for Make.
				if(td2) {
					make = td2.innerHTML.toLowerCase();
				}

				if (vin.indexOf(keyword) > -1 || make.indexOf(keyword) > -1) {
					tr[i].style.display = "";
				} else {
				    tr[i].style.display = "none";
				}
			}

			break;
		case 'customers':
			currentTable = document.getElementById("customers-table");
			tr = currentTable.getElementsByTagName("tr");
			for (i = 1; i < tr.length; i++) {
				var tdID = tr[i].getElementsByTagName("td")[0];
				var tdLastName = tr[i].getElementsByTagName("td")[2];
				var id = "";
				var lastname = "";

				// Search for ID.
				if(tdID) {
					id = tdID.innerHTML.toLowerCase();
				}

				// Search for Make.
				if(tdLastName) {
					lastname = tdLastName.innerHTML.toLowerCase();
				}

				if (id.indexOf(keyword) > -1 || lastname.indexOf(keyword) > -1) {
					tr[i].style.display = "";
				} else {
				    tr[i].style.display = "none";
				}
			}

			break;
		case 'today':
			currentTable = document.getElementById("today-table");
			tr = currentTable.getElementsByTagName("tr");
			for (i = 1; i < tr.length; i++) {
				var tdLastName = tr[i].getElementsByTagName("td")[1];
				var tdUnitNumber = tr[i].getElementsByTagName("td")[2];
				var lastname = "";
				var unit_number = "";

				// Search for ID.
				if(tdLastName) {
					lastname = tdLastName.innerHTML.toLowerCase();
				}

				// Search for Make.
				if(tdUnitNumber) {
					unit_number = tdUnitNumber.innerHTML.toLowerCase();
				}

				if (lastname.indexOf(keyword) > -1 || unit_number.indexOf(keyword) > -1) {
					tr[i].style.display = "";
				} else {
				    tr[i].style.display = "none";
				}
			}
			break;
		case 'history':
			currentTable = document.getElementById("history-table");
			tr = currentTable.getElementsByTagName("tr");
			for (i = 1; i < tr.length; i++) {
				var tdLastName = tr[i].getElementsByTagName("td")[1];
				var tdUnitNumber = tr[i].getElementsByTagName("td")[2];
				var lastname = "";
				var unit_number = "";

				// Search for ID.
				if(tdLastName) {
					lastname = tdLastName.innerHTML.toLowerCase();
				}

				// Search for Make.
				if(tdUnitNumber) {
					unit_number = tdUnitNumber.innerHTML.toLowerCase();
				}

				if (lastname.indexOf(keyword) > -1 || unit_number.indexOf(keyword) > -1) {
					tr[i].style.display = "";
				} else {
				    tr[i].style.display = "none";
				}
			}
			break;
	}
}

// ---------------------------------------------------------------------------------------------
var currentScreen = 'cars';

function UpdateCustomer(){
    var body = document.getElementById("customers-table");
	for(var i = body.rows.length-1; i > 0; i--){
		if(body.rows[i].style.background === "yellow"){
		    document.forms["update-customer-form"]["ufirst-name"].value = body.rows[i].cells[1].innerHTML;
		    document.forms["update-customer-form"]["ulast-name"].value = body.rows[i].cells[2].innerHTML;
		    document.forms["update-customer-form"]["uphone"].value = body.rows[i].cells[3].innerHTML;
		    document.forms["update-customer-form"]["uemail"].value = body.rows[i].cells[4].innerHTML;
            break;
		}
	}
}
function UpdateJobs(){
    var body = document.getElementById("today-table");
        
	for(var i = body.rows.length-1; i >= 0; i--){
		if(body.rows[i].style.background === "yellow"){
			document.forms["default-form"]["d-first-name"].value = body.rows[i].cells[0].innerHTML;
			document.forms["default-form"]["d-last-name"].value = body.rows[i].cells[1].innerHTML;
			document.forms["default-form3"]["d-start"].value = body.rows[i].cells[3].innerHTML;
			document.forms["default-form3"]["d-end"].value = body.rows[i].cells[3].innerHTML;
			break;
		}
	}
}

function UpdateHistory() {
    var body = document.getElementById("history-table");

    for (var i = body.rows.length - 1; i >= 0; i--) {
        if (body.rows[i].style.background === "yellow") {
            document.forms["update-history-form"]["uufirst-name"].value = body.rows[i].cells[0].innerHTML;
            document.forms["update-history-form"]["uulast-name"].value = body.rows[i].cells[1].innerHTML;
            document.forms["update-history-form"]["uuunit"].value = body.rows[i].cells[2].innerHTML;
            document.forms["update-history-form"]["uurental"].value = body.rows[i].cells[3].innerHTML;
            //document.forms["update-history-form"]["uucar"].value = body.rows[i].cells[4].innerHTML;
            document.forms["update-history-form"]["uubalance"].value = body.rows[i].cells[5].innerHTML;
            break;
        }
    }
}

function HistoryConfirmUpdate() {
    var first = document.forms["update-history-form"]["uufirst-name"].value;
    var last = document.forms["update-history-form"]["uulast-name"].value;
    var unit = document.forms["update-history-form"]["uuunit"].value;
    var time = document.forms["update-history-form"]["uurental"].value;
    //var body = document.forms["update-history-form"]["uucar"].value;
    var bal = document.forms["update-history-form"]["uubalance"].value;
    var splitTime = time.split(" - ");
    var beg = ConvertDateBack(splitTime[0]);
    var end = ConvertDateBack(splitTime[1]);
    var tidcid = GetTIDCID(unit, beg, end);
    console.log(tidcid[0], tidcid[1]);
    updateTransaction(tidcid[0], tidcid[1], unit, 3, 0, beg, end, 0, bal, "", "", "", function (cb) { });
    updateCustomer(tidcid[1], first, last, "", "", "", function(cb) {});
    document.getElementById("update-window-history").click();
    document.getElementById("refresh-button").click();
}
var tidArray = [];
function GetTIDCID(unit, beg, end) {
    for (var i = 0; i < tidArray.length; i++) {
        if (tidArray[i]["VIN"] === unit && tidArray[i]["StartDate"] === beg && tidArray[i]["EndDate"] === end) {
            return [tidArray[i]["TID"], tidArray[i]["CID"]];
        }
    }
}

/* update rows in today's job table to
 * transaction history table
 */
function UpdateCars() {
    var body = document.getElementById("cars-table");
        
	for(var i = body.rows.length-1; i >= 0; i--){
		if(body.rows[i].style.background === "yellow"){
			document.forms["update-car-form"]["uvin"].value = body.rows[i].cells[0].innerHTML;
			document.forms["update-car-form"]["umake"].value = body.rows[i].cells[1].innerHTML;
			document.forms["update-car-form"]["umodel"].value = body.rows[i].cells[2].innerHTML;
			document.forms["update-car-form"]["ubody-type"].value = body.rows[i].cells[3].innerHTML;
			document.forms["update-car-form"]["ucolour"].value = body.rows[i].cells[4].innerHTML;
			document.forms["update-car-form"]["uclass"].value = body.rows[i].cells[5].innerHTML;
			document.forms["update-car-form"]["ubranch"].value = body.rows[i].cells[6].innerHTML;
			break;
		}
	}
}

function CustomerConfirmUpdate() {
    var body = document.getElementById("customers-table");
    var cid;
    for (var i = body.rows.length - 1; i >= 0; i--) {
        if (body.rows[i].style.background === "yellow") {
            cid = body.rows[i].cells[0].innerHTML;
            break;
        }
    }
    var first = document.forms["update-customer-form"]["ufirst-name"].value;
    var last = document.forms["update-customer-form"]["ulast-name"].value;
    var phone = document.forms["update-customer-form"]["uphone"].value;
    var email = document.forms["update-customer-form"]["uemail"].value;
    updateCustomer(cid, first, last, phone, email, "", function () { });
    document.getElementById("update-window-customer").click();
    document.getElementById("refresh-button").click();
}
function RefreshAll() {
    RefreshCustomerTable();
    RefreshHistoryTable();
    RefreshTodayTable();
}
function UpdateWindow() {
	
    var updateScreen;
    var body;
    switch(currentScreen){
	    case 'cars':
	        return;
	    break;
	    case 'customers':
		    body = document.getElementById("customers-table");
	    break;
        case 'today':
            return;
	    case 'history':
		    body = document.getElementById("history-table");
		    break;
    }
    var set = false;
    for (var i = body.rows.length - 1; i > 0; i--) {
        if (body.rows[i].style.background === "yellow") {
            set = true;
            break;
        }
    }
    if (!set) {
        alert("Please select a row");
        return;
    }
    switch(currentScreen){
        case 'cars':
            return;
	    case 'customers':
		    updateScreen = document.getElementById("update-window-customer");
            UpdateCustomer();
		    break;
        case 'history':
            updateScreen = document.getElementById("update-window-history");
            UpdateHistory();
            break;
	    case 'today':
	        return;
    }       
    updateScreen.style.display = "block";
    window.onclick = function(event){
	    if (event.target === updateScreen){
		    updateScreen.style.display="none";
	    }
    };
    var span = document.getElementsByClassName("add-close");
    for (var item in span) {
	    span[item].onclick = function(){
		    updateScreen.style.display = "none";
			isupdate = false;
	    };
    }

    //for (var i = body.rows.length - 1; i >= 0; i--) {
    //    if (body.rows[i].style.background === "yellow") {
    //        isupdate = true;
    //    }
    //    if (isupdate === false) {
    //        return;
    //    }
    //}
}

// Add window pop up
function AddWindow() {
	//var update = document.getElementById("update-pop-up");
	var addScreen;
	switch(currentScreen){
		case 'cars':
			addScreen = document.getElementById("add-window-cars");
			break;
		case 'customers':
			addScreen = document.getElementById("add-window-customer");
			break;
		default:
			addScreen = document.getElementById("add-window-default");
			break;
	}
	addScreen.style.display = "block";
	
	window.onclick = function(event){
		if (event.target === addScreen){
			addScreen.style.display="none";
		}
	};
	var span = document.getElementsByClassName("add-close");
	for (var item in span) {
		span[item].onclick = function(){
			addScreen.style.display = "none";
		};
	}
}

// Push data to server
function PushData(postData, remote) {
	var post = new FormData();
	post.append('action', remote);
	for (var i in postData){
		//console.log(i, postData[i]); //remove later
		post.append(i, postData[i]);
	}

	outPost(post, "http://4dudes.servebeer.com/backend/addhandler.py", function (cbt) { console.log(cbt); });
}

// Parse car form and update server
function ParseCarForm() {
	var vin = document.getElementById("vin").value;
	var make = document.getElementById("make").value;
	var model = document.getElementById("model").value;
	var bodyType = document.getElementById("body-type").value;
	var colour = document.getElementById("colour").value;
	var clas = parseInt(document.getElementById("class").value);
	var branch = parseInt(document.getElementById("branch").value);

	var data = {"Model" : model, "Colour" : colour, "VIN" : vin, "Class" : clas, "Make" : make, "AtBranch" : branch, "BodyStyle" : bodyType};
	var postData = {"model" : model, "colour" : colour, "vin" : vin, "class" : clas, "make" : make, "atbranch" : branch, "bodystyle" : bodyType};
	PushData(postData, 'cars');
	return data;
}

// Add car form to table
function AddCarForm() {
	var data = ParseCarForm();
	AddCarRow(data);
	document.getElementById("car-form").reset();
	document.getElementById("add-window-cars").click();
        if (isupdate === true){
            DeleteRow();
            isupdate = false;
        }
}

// Returns a table element given a tableID, row, and column
function GetTableElement(tableId, row, column) {
	table = document.getElementById(tableId);
	return table.rows[row].cells[column].innerHTML;
}

// Parse customer form and update server
// returns same entry from server to get cid
function ParseCustomerForm() {
	var fn = document.getElementById("first-name").value;
	var ln = document.getElementById("last-name").value;
	var phone = document.getElementById("phone").value;
	var email = document.getElementById("email").value;
	var postData = {"firstname": fn, "lastname": ln, "phonenumber": phone, "email": email, "password": "password"};
	PushData(postData, 'customers');
	var data = {"FirstName": fn, "LastName": ln, "PhoneNumber": phone, "Email": email };
	return data;
}

// Add customer to Transaction table
function AddCustomerForm() {
    var data = ParseCustomerForm();
    //AddCustomerRow(data);
    RefreshCustomerTable();
    document.getElementById("customer-form").reset();
    document.getElementById("add-window-customer").click();
        if (isupdate === true){ //? yun, why is this here?
            DeleteRow();
            isupdate = false;
        }
}

// Test() is just a test function to prove database retrieval
function Test (remote) {
	var data = new FormData();
	data.append("action", remote);
	cb = function(cbt) {
		obj = JSON.parse(cbt);
		console.log(cbt);
	}
	outPost(data, "http://4dudes.servebeer.com/backend/handler.py", cb);
}

// Pull and populate customer data
function PullCustomersData () {
	var data = new FormData();
	data.append('action', 'customers');
	outPost(data, "http://4dudes.servebeer.com/backend/handler.py", FillCustomerData);
}

// Add row to customer table (helper function)
function AddCustomerRow (data) {
	var table = document.getElementById("customers-table");
	var row = table.insertRow(1);
	row.ondblclick= function() { GetRow(table); };
	row.onclick = function () { ClickOnRow(row, "customers-table", "yellow"); };
	row.onmousemove = row.style.cursor="pointer";
	row.style.background="white";
	var values = ['CID', 'FirstName', 'LastName', 'PhoneNumber', 'Email'];
	for (var i in values) {
		var cell = row.insertCell();
		cell.innerHTML = data[values[i]];
	}
	if (parseInt(data["CID"]) > recentCID) { recentCID = parseInt(data["CID"]); }

}

function RefreshCustomerTable() {
    ClearTable("customers-table");
    PullCustomersData();
}

function RefreshHistoryTable() {
    ClearTable("history-table");
    PullHistoryData();
}

// Add row to customer table (helper function)
function FillCustomerData (string) {
	var obj = JSON.parse(string);
	for (var i in obj) {
		AddCustomerRow(obj[i]);
	}
}

// Pull and populate table
function PullCarsData () {
	var data = new FormData();
	data.append('action', 'cars');
	outPost(data, "http://4dudes.servebeer.com/backend/handler.py", FillCarsData);
}

// Add row to cars
function AddCarRow (data) {
	var table = document.getElementById("cars-table");
	var row = table.insertRow(1);
	row.ondblclick= function() { GetRow(table); };
	row.onclick = function () { ClickOnRow(row, "cars-table", "yellow"); };
	row.onmousemove = row.style.cursor="pointer";
	row.style.background="white";
	var values = ['VIN', 'Make', 'Model', 'BodyStyle', 'Colour', 'Class', 'AtBranch'];
	for (var i in values) {
		var cell = row.insertCell();
		cell.innerHTML = data[values[i]];
	}
}

// Fill data to table
function FillCarsData (string) {
	var obj = JSON.parse(string);
	for (var i in obj) {
		AddCarRow(obj[i]);
	}
}

var historyData;
//dead-end function just to set the global historyData var
function FillHistoryData(string) {
    tidArray = [];
    historyData = null;
    var obj = JSON.parse(string);
    for (var i in obj) {
        var balance = parseFloat(obj[i]["AmountDue"]) - parseFloat(obj[i]["AmountPaid"]);
        var rentalTime = ConvertDates(obj[i]["StartDate"]) + " - " + ConvertDates(obj[i]["EndDate"]);
        historyData = { 'VIN': obj[i]["Car"], 'RentalTime': rentalTime, 'Balance': balance }
        HistoryDataWrapper(obj[i]["Car"], obj[i]["Customer"]);
        tidArray.push({ 'CID' : obj[i]["Customer"], 'TID': obj[i]["TID"], "VIN": obj[i]["Car"], "StartDate": obj[i]["StartDate"], "EndDate": obj[i]["EndDate"]});
    }
}

// wrapper function that gets the car body from local data (local because it's just easier)
// and customer name
function HistoryDataWrapper(vin, cid) {
    var carTable = document.getElementById("cars-table");
    for (var i = 0; i < carTable.rows.length; i++) {
        if (carTable.rows[i].cells[0].innerHTML === vin) {
            historyData['BodyStyle'] = carTable.rows[i].cells[3].innerHTML;
        }(i)
    }
    var customerTable = document.getElementById("customers-table");
    for (var i = 0; i < customerTable.rows.length; i++) {
        if (parseInt(customerTable.rows[i].cells[0].innerHTML) === parseInt(cid)) {
            historyData['FirstName'] = customerTable.rows[i].cells[1].innerHTML;
            historyData['LastName'] = customerTable.rows[i].cells[2].innerHTML;
        }(i)
    }
    AddHistoryRow(historyData);
}

// Add row to transactions
function AddHistoryRow (data) {
	var table = document.getElementById("history-table");
	var row = table.insertRow(1);
	row.ondblclick= function() { GetRow(table); };
	row.onclick = function () { ClickOnRow(row, "history-table", "yellow"); };
	row.onmousemove = row.style.cursor="pointer";
	row.style.background="white";
	var values = ['FirstName', 'LastName', 'VIN', 'RentalTime', 'BodyStyle', 'Balance'];
	for (var i in values) {
		var cell = row.insertCell();
		cell.innerHTML = data[values[i]];
	}
}

function PullHistoryData() {
    var data = new FormData();
    data.append('action', 'transactions');
    outPost(data, "http://4dudes.servebeer.com/backend/handler.py", FillHistoryData);
}

// Runs functions on reload/load
function onload () {
	PullCarsData();
	PullCustomersData();
	PullHistoryData();
	SetTodaysDate();
}


document.onload = onload();