#!/usr/bin/python3
import auth

auth.header_text()

# Check that the table is correct, and delete the specified row
def delete(table, ident, cursor):
    table_ident = ""
    if table == "Branches":
        table_ident = "BID"
    elif table == "CarClasses":
        table_ident = "ClassID"
    elif table == "Cars":
        table_ident = "VIN"
    elif table == "Customers":
        table_ident = "CID"
    elif table == "Employees":
        table_ident = "EID"
    else:
        print(0)
        exit()

    cursor.execute("DELETE FROM %s WHERE %s = %%s" % (table, table_ident), ident)

# Get data and pipe it into the correct function
def main():
    form = auth.cgi.FieldStorage()
    cursor = auth.get_cursor()
    action = form.getfirst("action")
    ident = form.getfirst("id")

    if not action or not ident:
        print(0)
        exit()

    delete(action, ident, cursor)
    print(1)

main()
