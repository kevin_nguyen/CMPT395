/*
 * Get data from a server using ajax
 *
 * Data is an array of values to be passed to the server script
 * Always include a key 'action' with one of the following values, depending
 * on what you want to recieve:
 * cars, classes, customers, transactions, branches, or employees
 * The only useful value for any action other than transactions is 'ids'
 * If using transactions as the action you have the following options:
 * cid, vin, rentedfrom, returnedto, startdate,
 * enddate, status, rentalby, returnedby
 *
 * For this project use "4dudes.servebeer.com/backend/handler.py" as loc
 *
 * cb is a function to be run when data is returned, for example
 * cb = function(cbt) {
 * 		obj = json.parse(cbt)
 * 		console.log(cbt)
 * }
 */
function outPost(data, loc, cb) {
	var XHR = new XMLHttpRequest();
	var name;
	var out = document.getElementById("output");

	XHR.open("POST", loc, false);
	XHR.onreadystatechange = function () {
		if (XHR.readyState === 4 && XHR.status === 200) {
			if (typeof cb === 'function') {
				cb(XHR.responseText);
			} else {
				console.log(XHR.responseText);
			}
		}
	};

	XHR.send(data);

}

/*
 * For both checkCustomerLogin and checkEmployeeLogin:
 * the first parameter is the login identifier, Email for customers and eid for employees.
 * the second parameter is the password
 * the final parameter is a function to be executed when the password is retrieved
 *
 * the cb function should have exactly one parameter, this will be an integer
 * if it is 1, the login succeeded, otherwise it failed
 * to easily check this, use if (parameter != "1")
 * if this statement is true, then the login has failed. If the above statement
 * is false then the login succeeded
 */

function checkCustomerLogin(email, password, cb) {
	data = new FormData();
	data.append("action", "customer");
	data.append("email", email);
	data.append("password", password);

	outPost(data, "http://4dudes.servebeer.com/backend/loginhandler.py", cb);
}

function checkEmployeeLogin(eid, password, cb) {
	data = new FormData();
	data.append("action", "employee");
	data.append("eid", eid);
	data.append("password", password);

	outPost(data, "http://4dudes.servebeer.com/backend/loginhandler.py", cb);
}

function updateCustomer(cid, first, last, phone, email, pass, cb) {
	data = new FormData();
	if (!cid) {
		return;
	}
    console.log(cid)
	data.append("CID", cid);

	if (first != "")
		data.append("FirstName", first);
	if (last != "")
		data.append("LastName", last);
	if (phone != "")
		data.append("PhoneNumber", phone);
	if (email != "")
		data.append("Email", email);
	if (pass != "")
		data.append("Password", pass);

	outPost(data, "http://4dudes.servebeer.com/backend/updatecustomer.py", cb);
}

function updateEmployee(eid, first, last, manager, pass, cb) {
	data = new FormData();
	if (!eid) {
		return;
	}

	data.append("EID", eid);

	if (first != "")
		data.append("FirstName", first);
	if (last != "")
		data.append("LastName", last);
	if (manager != "")
		data.append("PhoneNumber", phone);
	if (pass != "")
		data.append("Password", pass);

	outPost(data, "http://4dudes.servebeer.com/backend/updateemployee.py", cb);
}

function updateTransaction(tid, customer, car, rentedfrom, returnedto, startdate, enddate, stat, amountdue, amountpaid, rentalby, returnedby, cb) {
	data = new FormData();
	if (!tid) {
		return;
	}

	data.append("TID", tid);

	if (customer != "")
		data.append("Customer", customer);
	if (car != "")
		data.append("Car", car);
	if (rentedfrom != "")
		data.append("RentedFrom", rentedfrom);
	if (returnedto != "")
		data.append("ReturnedTo", returnedto);
	if (startdate != "")
		data.append("StartDate", startdate);
	if (enddate != "")
		data.append("EndDate", enddate);
	if (stat != "")
		data.append("Status", stat);
	if (amountdue != "")
		data.append("AmountDue", amountdue);
	if (amountpaid != "")
		data.append("AmountPaid", amountpaid);
	if (rentalby != "")
		data.append("RentalBy", rentalby);
	if (returnedby != "")
		data.append("ReturnedBy", returnedby);

	outPost(data, "http://4dudes.servebeer.com/backend/updatetransaction.py", cb);
}
