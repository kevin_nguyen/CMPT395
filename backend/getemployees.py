#!/usr/bin/python3
import auth

# Print a json object of either all employees if no ids are specified,
# all employees specified if ids are given.
# ids must be sent as either GET or POST with tag 'ids'
def main(form, cursor):
    ids = form.getlist("ids")

    if not ids:
        cursor.execute("SELECT * FROM Employees")
    else:
        cursor.executemany("SELECT * FROM Employees WHERE EID = %s", ids)

    data = cursor.fetchall()
    print(auth.json.dumps(data));
