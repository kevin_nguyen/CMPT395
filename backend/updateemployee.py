#!/usr/bin/python3
import auth

auth.header_text()

def main():
    form = auth.cgi.FieldStorage()
    cursor = auth.get_cursor()

    if not form.getfirst("EID"):
        print("-1", end='')
        return

    query = "UPDATE Employees SET"
    variables = []

    for key in ("FirstName", "LastName", "IsManager", "Password"):
        if form.getfirst(key):
            if not key == "FirstName":
                query += ","
            query += " "
            query += key
            query += " = %s"
            variables.append(form.getfirst(key))

    query += " WHERE EID = %s"
    variables.append(form.getfirst("EID"))

    cursor.execute(query, variables)
    print("1", end='')
    return

main()
