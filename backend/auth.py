# Keep all imports in one place for simplicity
import django
import pymysql
import json
import cgi
import atexit
import cgitb
cgitb.enable()

# Database connection
db = pymysql.connect("localhost", "rental",
        "rental", "395_rental_db")

# Print the header for text/html
def header_text():
    print("Access-Control-Allow-Origin: *")
    print("Content-Type: text/html")
    print()

# Get a database cursor
def get_cursor():
    return db.cursor(pymysql.cursors.DictCursor)

def get_form():
    return form

# End the database connection
def cleanup():
    db.commit()
    db.close()

atexit.register(cleanup)
