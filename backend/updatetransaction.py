#!/usr/bin/python3
import auth

auth.header_text()

def main():
    form = auth.cgi.FieldStorage()
    cursor = auth.get_cursor()

    if not form.getfirst("TID"):
        print("-1", end='')
        return

    query = "UPDATE Transactions SET"
    variables = []

    for key in ("Customer", "Car", "RentedFrom", "ReturnedTo", "StartDate", "EndDate", "Status", "AmountDue", "AmountPaid", "RentalBy", "ReturnedBy"):
        if form.getfirst(key):
            if not key == "Customer":
                query += ","
            query += " "
            query += key
            query += " = %s"
            variables.append(form.getfirst(key))

    query += " WHERE TID = %s"
    variables.append(form.getfirst("TID"))

    cursor.execute(query, variables)
    print("1", end='')
    return

main()
