#!/usr/bin/python3
import auth

# Print a json object of either all car classes if no ids are specified,
# all car classes specified if ids are given.
# ids must be sent as either GET or POST with tag 'ids'
def main(form, cursor):
    ids = form.getlist("ids")

    if not ids:
        cursor.execute("SELECT * FROM CarClass")
    else:
        cursor.executemany("SELECT * FROM CarClass WHERE ClassId = %s", ids)

    data = cursor.fetchall()
    print(auth.json.dumps(data));
