#!/usr/bin/python3
import auth

auth.header_text()

# Check if logins for customers are valid
# Customers must give their Email and password
def customerLogin(email, password, cursor):
    if not email or not password:
        print(0)
        exit()

    cursor.execute("SELECT * FROM Customers WHERE Email = %s AND Password = %s", (email, password))

    data = cursor.fetchone()
    if len(data) > 0:
        print(data['CID'])
    else:
        print(0)

# Check if logins for employees are valid
# Employees must give their EID and password
def employeeLogin(eid, password, cursor):
    if not eid or not password:
        print(0)
        exit()

    cursor.execute("SELECT * FROM Employees WHERE EID = %s AND Password = %s", (eid, password))

    data = cursor.fetchone()
    if len(data) > 0:
        print(data['EID'])
    else:
        print(0)

# Get data and pipe it into the correct function
def main():
    form = auth.cgi.FieldStorage()
    cursor = auth.get_cursor()
    action = form.getfirst("action")

    if not action:
        print(0)
        exit()

    if action == "customer":
        email = form.getfirst("email")
        password = form.getfirst("password")
        customerLogin(email, password, cursor)
    elif action == "employee":
        eid = form.getfirst("eid")
        password = form.getfirst("password")
        employeeLogin(eid, password, cursor)

main()
