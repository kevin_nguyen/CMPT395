#!/usr/bin/python3
import auth

# Create a transaction
# requires customer, car, rentedfrom, startdate, enddate, amountdue, rentalby
def main(form, cursor):
    Customer = form.getfirst("customer")
    Car = form.getfirst("car")
    RentedFrom = form.getfirst("rentedfrom")
    StartDate = form.getfirst("startdate")
    EndDate = form.getfirst("enddate")
    AmountDue = form.getfirst("amountdue")

    if not Customer or not Car or not RentedFrom or not StartDate or not EndDate or not AmountDue:
        print(-1)
        exit();

    cursor.execute("INSERT INTO Transactions (Customer, Car, RentedFrom, StartDate, EndDate, AmountDue) VALUES (%s, %s, %s, %s, %s, %s)", (Customer, Car, RentedFrom, StartDate, EndDate, AmountDue))

    print(cursor.lastrowid)
