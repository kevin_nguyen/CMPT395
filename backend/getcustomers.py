#!/usr/bin/python3
import auth

# Print a json object of either all customers if no ids are specified,
# all customers specified if ids are given.
# ids must be sent as either GET or POST with tag 'ids'
def main(form, cursor):
    ids = form.getlist("ids")

    if not ids:
        cursor.execute("SELECT * FROM Customers")
    else:
        cursor.executemany("SELECT * FROM Customers WHERE CID = %s", ids)

    data = cursor.fetchall()
    print(auth.json.dumps(data));
