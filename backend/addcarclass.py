#!/usr/bin/python3
import auth

# Create a car class
# requires name, dailycost, weeklycost, monthlycost
def main(form, cursor):
    Name = form.getfirst("name")
    DailyCost = form.getfirst("dailycost")
    WeeklyCost = form.getfirst("weeklycost")
    MonthlyCost = form.getfirst("monthlycost")

    if not Name or not DailyCost or not WeeklyCost or not MonthlyCost:
        print(-1)
        exit();

    cursor.execute("INSERT INTO CarClasses (Name, DailyCost, WeeklyCost, MonthlyCost) VALUES (%s, %s, %s, %s)", (Name, DailyCost, WeeklyCost, MonthlyCost))

    print(cursor.lastrowid)
