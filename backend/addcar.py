#!/usr/bin/python3
import auth

# Create a car
# requires vin, make, model, bodystyle, colour, and class
def main(form, cursor):
    VIN = form.getfirst("vin")
    Make = form.getfirst("make")
    Model = form.getfirst("model")
    BodyStyle = form.getfirst("bodystyle")
    Colour = form.getfirst("colour")
    Class = form.getfirst("class")
    AtBranch = form.getfirst("atbranch")

    if not VIN or not Make or not Model or not BodyStyle or not Colour or not Class or not AtBranch:
        print(-1, end="")
        exit();

    cursor.execute("INSERT INTO Cars (VIN, Make, Model, BodyStyle, Colour, Class, AtBranch) VALUES (%s, %s, %s, %s, %s, %s, %s)", (VIN, Make, Model, BodyStyle, Colour, Class, AtBranch))

    print(cursor.lastrowid)
