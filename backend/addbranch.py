#!/usr/bin/python3
import auth

# Create a branch
# requires location
def main(form, cursor):
    Location = form.getfirst("location")

    if not Location:
        print(-1)
        exit();

    cursor.execute("INSERT INTO Branches (Location) VALUES (%s)", (Location))

    print(cursor.lastrowid)
