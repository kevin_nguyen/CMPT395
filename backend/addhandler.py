#!/usr/bin/python3
import auth

auth.header_text()

# Insert values into the database
# values must be sent as either add or POST with
# tags specified in the files
def main():
    form = auth.cgi.FieldStorage()
    cursor = auth.get_cursor()
    action = form.getfirst("action")

    if not action:
        print("-1", end='')
        return;
    else:
        if action == "cars":
            import addcar
            addcar.main(form, cursor)
        elif action == "classes":
            import addcarclass
            addcarclass.main(form, cursor)
        elif action == "customers":
            import addcustomer
            addcustomer.main(form, cursor)
        elif action == "transactions":
            import addtransaction
            addtransaction.main(form, cursor)
        elif action == "branches":
            import addbranch
            addbranch.main(form, cursor)
        elif action == "employees":
            import addemployee
            addemployee.main(form, cursor)
        else:
            print("-1", end='')

main()
