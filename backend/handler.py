#!/usr/bin/python3
import auth

auth.header_text()

# Print a json object of either all cars if no ids are specified,
# all cars specified if ids are given.
# ids must be sent as either GET or POST with tag 'ids'
def main():
    form = auth.cgi.FieldStorage()
    cursor = auth.get_cursor()
    actions = form.getlist("action")

    if not actions:
        print("{}", end='')
        return;
    else:
        for action in actions:
            if action == "cars":
                import getcars
                getcars.main(form, cursor)
            elif action == "classes":
                import getcarclasses
                getcarclasses.main(form, cursor)
            elif action == "customers":
                import getcustomers
                getcustomers.main(form, cursor)
            elif action == "transactions":
                import gettransactions
                gettransactions.main(form, cursor)
            elif action == "branches":
                import getbranches
                getbranches.main(form, cursor)
            elif action == "employees":
                import getemployees
                getemployees.main(form, cursor)
            else:
                print("{}", end='')

main()
