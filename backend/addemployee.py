#!/usr/bin/python3
import auth

# Create an employee
# requires firstname, lastname, ismanager, password
def main(form, cursor):
    FirstName = form.getfirst("firstname")
    LastName = form.getfirst("lastname")
    IsManager = form.getfirst("ismanager")
    Password = form.getfirst("password")

    if not FirstName or not LastName or not IsManager or not Password:
        print(-1)
        exit();

    cursor.execute("INSERT INTO Employees (FirstName, LastName, IsManager, Password) VALUES (%s, %s, %s, %s)", (FirstName, LastName, IsManager, Password))

    print(cursor.lastrowid)
