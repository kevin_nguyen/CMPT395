#!/usr/bin/python3
import auth

auth.header_text()

def main():
    form = auth.cgi.FieldStorage()
    cursor = auth.get_cursor()

    if not form.getfirst("CID"):
        print("-1", end='')
        return

    query = "UPDATE Customers SET"
    variables = []

    for key in ("FirstName", "LastName", "PhoneNumber", "Email", "Password"):
        if form.getfirst(key):
            if not key == "FirstName":
                query += ","
            query += " "
            query += key
            query += " = %s"
            variables.append(form.getfirst(key))

    query += " WHERE CID = %s"
    variables.append(form.getfirst("CID"))

    cursor.execute(query, variables)
    print("1", end='')
    return

main()
