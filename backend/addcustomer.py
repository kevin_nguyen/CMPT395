#!/usr/bin/python3
import auth

# Create a customer
# requires firstname, lastname, phonenumber, email, password
def main(form, cursor):
    FirstName = form.getfirst("firstname")
    LastName = form.getfirst("lastname")
    PhoneNumber = form.getfirst("phonenumber")
    Email = form.getfirst("email")
    Password = form.getfirst("password")

    if not FirstName or not LastName or not PhoneNumber or not Email or not Password:
        print(-1)
        exit();

    cursor.execute("INSERT INTO Customers (FirstName, LastName, PhoneNumber, Email, Password) VALUES (%s, %s, %s, %s, %s)", (FirstName, LastName, PhoneNumber, Email, Password))

    print(cursor.lastrowid)
