#!/usr/bin/python3
import auth

# Print a json object of either all transactions if no parameters are specified,
# all transactions specified if parameters are given.
# only the first of each parameter will be used
# parameters must be sent as either GET or POST with the following tags
# 'cid', for the customer
# 'vin', for the vehicle
# 'rentedfrom', for the branch rented from
# 'returnedto', for the branch returned to
# 'startdate', for the date at which the transaction begins
# 'enddate', for the date at which the transaction ends
# 'status', for the current status of the transaction
# 'rentalby', for the employee who finalized the rental
# 'returnedby', for the employee who recieved the vehicle
def main(form, cursor):
    fields = {};
    fields["Customer"] = form.getfirst("cid")
    fields["Car"] = form.getfirst("vin")
    fields["RentedFrom"] = form.getfirst("rentedfrom")
    fields["ReturnedTo"] = form.getfirst("returnedto")
    fields["StartDate"] = form.getfirst("startdate")
    fields["EndDate"] = form.getfirst("enddate")
    fields["Status"] = form.getfirst("status")
    fields["RentalBy"] = form.getfirst("rentalby")
    fields["ReturnedBy"] = form.getfirst("returnedby")

    query = "SELECT * FROM Transactions"
    started = False
    args = []

    for field in fields:
        if not fields[field]:
            continue
        if not started:
            started = True;
            query += " WHERE"
        query += " " + field + " = %s"
        args.append(fields[field])

    cursor.execute(query, args)
    data = cursor.fetchall()
    print(auth.json.dumps(data));
