#!/usr/bin/python3
import auth

auth.header_text()

def customers():
    cursor = auth.get_cursor()
    transactions = cursor.execute("SELECT * FROM Transactions")

    t_data = cursor.fetchall()
    c_ids = {}

    for row in t_data:
        if not row["Customer"] in c_ids:
            c_ids[row["Customer"]] = 1
        else:
            c_ids[row["Customer"]] += 1

    c_sort = [(cust, c_ids[cust]) for cust in sorted(c_ids, key = c_ids.get, reverse=True)]
    for cust, num in c_sort:
        cursor.execute("SELECT FirstName, LastName, Email FROM Customers WHERE CID = %s", cust)
        c_data = cursor.fetchone()
        print(str(c_data["FirstName"]) + " " + str(c_data["LastName"]) + " " + str(c_data["Email"]) + ": " + str(num))

def branches():
    cursor = auth.get_cursor()
    transactions = cursor.execute("SELECT * FROM Transactions")

    t_data = cursor.fetchall()
    b_ids = {}

    for row in t_data:
        if not row["RentedFrom"] in b_ids:
            b_ids[row["RentedFrom"]] = 1
        else:
            b_ids[row["RentedFrom"]] += 1

    b_sort = [(bran, b_ids[bran]) for bran in sorted(b_ids, key = b_ids.get, reverse=True)]
    for bran, num in b_sort:
        cursor.execute("SELECT Location FROM Branches WHERE BID = %s", bran)
        b_data = cursor.fetchone()
        print(str(b_data["Location"]) + ": " + str(num))

form = auth.cgi.FieldStorage()
if not form.getfirst('action'):
    exit()

if form.getfirst('action') == "customers":
    customers()
elif form.getfirst('action') == "branches":
    branches()
