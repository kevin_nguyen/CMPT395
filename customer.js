var currentScreen = 'date'
var backScreen = 'date'
var selectedCar;
var selectedDateStart;
var selectedDateEnd;
var selectedLocation;
var filters = [false, false, false, false,  false, false];
var logged = false;
var postTransaction = {};
var postCustomer = {};
var postCar = {};
var localCarCopy = {};
var localCarArray = [];
var largestCID = 0;
var initialLoad = true;
var getpull = false;
var globalemail;
var globalbranch;
var username;
var globalpassword;
var globaluserid;
var goldMemberShip = false;
var markers = [];
var locations = [
  ['Branch 1', 53.521192, -113.505769, "<div><img src='U of A branch.jpg' height='100px' width='150px'/><br>University Branch <div>Address: 116 St & 85 Ave,<br> Edmonton, AB T6G 2R3,<br> Hours:Open today 10AM-5PM<div><p><button onclick='disableMap(1)'>Choose this Branch</button></p></div></div></div>"],
  ['Branch 2', 53.488347, -113.503117, "<div><img src='Southgate branch.jpg' height='100px' width='150px'/><br>SouthGate Branch<div>Address: 5015 111 St NW,<br> Edmonton, AB T6H 4M6<br>Hours: Open today 9AM-9PM<div><p><button onclick='disableMap(2)'>Choose this Branch</button></p></div></div></div>"],
  ['Branch 3', 53.541885, -113.522061, "<div><img src='Jasper Branch.jpg' height='100px' width='150px'/><br>Jasper Branch<div>Address: 11020 Jasper Ave,<br> Edmonton, AB T5K 2N1<br>Hours: Open today 9AM-9PM<div><p><button onclick='disableMap(3)'>Choose this Branch</button></p></div></div></div>"],
  ['Branch 4', 53.542439, -113.490819, "<div><img src='DownTown Branch.jpg' height='100px' width='150px'/><br>Branch on 100<div>Address: 10025 102A Ave NW,<br> Edmonton, AB T5J 2Z2<br>Hours: Open today 9AM-9PM<div><p><button onclick='disableMap(4)'>Choose this Branch</button></p></div></div></div>"],
  ['Branch 5', 53.511938, -113.644238, "<div><img src='West Ed Branch.jpg' height='100px' width='150px'/><br>West ED Branch<div>Address: 8882 170 St NW,<br>Edmonton, AB T5T 4J2<br>Hours: Open today 9AM-9PM<div><p><button onclick='disableMap(5)'>Choose this Branch</button></p></div></div></div>"],
];

function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 11,
        center: { lat: 53.541885, lng: -113.503769 }
    });

    setMarkers(map);
}

function setMarkers(map) {
    for (var i = 0; i < locations.length; i++) {

        var location = locations[i];
        var locationInfowindow = new google.maps.InfoWindow({
            content: location[3],
        });

        var marker = new google.maps.Marker({
            position: { lat: location[1], lng: location[2] },
            map: map,
            title: location[0],
            infowindow: locationInfowindow
        });

        markers.push(marker);

        google.maps.event.addListener(marker, 'click', function () {
            hideAllInfoWindows(map);
            this.infowindow.open(map, this);
        });

    }
}
function hideAllInfoWindows(map) {
    markers.forEach(function (marker) {
        marker.infowindow.close(map, marker);
    });
}
function enableMap() {
    document.getElementById("map").style.display = "block";
    initMap();
}

function disableMap(branch) {
    ClearTable("car-table");
    globalbranch = branch;
    document.getElementById("map").style.display = "none";
    var string;
    switch (branch) {
        case 1:
            string = "University Branch";
            break;
        case 2:
            string = "SouthGate Branch";
            break;
        case 3:
            string = "Jasper Branch";
            break;
        case 4:
            string = "Branch on 100";
            break;
        case 5:
            string = "West ED Branch";
            break;
        default:
            break;
    }
    document.getElementById("choose-branch").value = string;
    SetLocation();
}

function ToggleLoginStatus() {
    logged = !logged;
    if (logged === false) {
        globalemail = null;
        globalpassword = null;
        globaluserid = null;
        RemoveData("account-info")
        document.getElementById("top-bar-right").style.display = "none";
        ResetTable(document.getElementById("trasaction-history-table"));
    }
}

function Login() {
    //GetUserInfo("signin-form");
    ToggleLoginStatus();
    SwitchScreen("account");
    PullTransactionInfo ();
}

function Signin() {
    GetUserInfo("signin-form");
    ToggleLoginStatus();
    SwitchScreen("account");
    //PullTransactionInfo ()
}

function CheckLogin(formname) {
    var email = document.getElementById(formname).elements[0].value;
    globalemail = email;
    var password = document.getElementById(formname).elements[1].value;
    globalpassword = password;
    checkCustomerLogin(email, password, LoginCheck);
}
function LoginConfirm() {
    if (logged === false ) {
        document.getElementById('confirm-login').style.display='block';
    } 
    else {
        //ParseNewTransaction();
        SwitchScreen('account');
        
    }
}
function LoginCheck(ctb) {
    if (ctb > 0) {
        PullCustomersInfo(globalemail);
        Login();
        SwitchTopBar();
        document.getElementById('log-in').style.display='none';
        document.getElementById("login-form").elements[0].style.border="1px solid buttonface";
        document.getElementById("login-form").elements[1].style.border="1px solid buttonface";
        document.getElementById("login-input-wrong").innerHTML = "";
        
    }
    else {
        document.getElementById("login-form").elements[0].style.border="1px solid red";
        document.getElementById("login-form").elements[1].style.border="1px solid red";
        document.getElementById("login-input-wrong").innerHTML = "Invalid email or password";
    }
}

function SigninCheck(ctb) {
    if (ctb > 0) {
        PullCustomersInfo(globalemail);
        Signin();
        SwitchTopBar();
        document.getElementById('sign-up').style.display='none';
    }
    else if (ctb === -1) {
        alert("You must fill every input field");
    }
    else {
        alert("Invalid email address or duplicated email address");
    }
}
function FillComfirmInfo(){
    document.getElementById("first-name").value = document.getElementById('account-info').getElementsByTagName("p")[0].innerHTML;
    document.getElementById("last-name").value = document.getElementById('account-info').getElementsByTagName("p")[1].innerHTML;
    document.getElementById("email").value = globalemail;
    document.getElementById("phone").value = document.getElementById('account-info').getElementsByTagName("p")[3].innerHTML;
}

function GetUserInfo(formname) {
    
    RemoveData("account-info");
    document.getElementById('account-save-button').style.display = "none";
    for (var i = 0; i < 4; i++) {
        if (formname === "account-change-form") {
            FillData(document.getElementById(formname).elements[i].value, "account-info");
        }
        /*else if (i === 1){
            continue;
        }*/
        else {
            FillData(document.getElementById(formname).elements[i].value, "account-info");
        }
    }
}

function ParseSignInForm(formname) {
    var fn = document.getElementById(formname).elements[0].value;
    var ln = document.getElementById(formname).elements[1].value;
    var email = document.getElementById(formname).elements[2].value;
    globalemail = email;
    var password = document.getElementById(formname).elements[3].value;
    var phone = document.getElementById(formname).elements[4].value;
    var postData = {"firstname": fn, "lastname": ln, "phonenumber": phone, "email": email, "password": password };
    PushData(postData, 'customers', SigninCheck);
}

function ParseCustomerSetting() {
    var firstname = document.getElementById("account-change-first").value;
    var lastname = document.getElementById("account-change-last").value;
    var pass = document.getElementById("account-change-email").value;
    var phone = document.getElementById("account-change-phone").value;
    updateCustomer(globaluserid, firstname, lastname, phone, globalemail, pass, CheckCustomerSetting);
}



function CheckCustomerSetting(ctb) {
    
}
function ParseNewTransaction() {
    var customer = document.getElementById("first-name").value;
    var car = document.getElementById('confirm-car').getElementsByTagName("p")[0].innerHTML;
    var branch = document.getElementById('confirm-date').getElementsByTagName("p")[2].innerHTML;
    var start = document.getElementById('confirm-date').getElementsByTagName("p")[0].innerHTML;;
    var end = document.getElementById('confirm-date').getElementsByTagName("p")[1].innerHTML;;
    var status = 2;
    var amount = 222.02;
    var rentalby = 1;
    var postData = {"customer": customer, "car": car, "rentedcrom": branch, "startdate": start, "enddate": end, "AmountDue": amount, "rentalby":rentalby};
    PushData(postData, 'transactions', TransactionCheck);
}

function TransactionCheck(ctb) {
    if (ctb > 0) {
        PullTransactionInfo ();
    }
    else {
        alert("something wrong with transmitting data");
    }
}
function FillAccountInput(formname) {
    document.getElementById("account-change-first").value = document.getElementById(formname).getElementsByTagName("p")[0].innerHTML;
    document.getElementById("account-change-last").value = document.getElementById(formname).getElementsByTagName("p")[1].innerHTML;
    document.getElementById("account-change-email").value = document.getElementById(formname).getElementsByTagName("p")[2].innerHTML;
    document.getElementById("account-change-phone").value = document.getElementById(formname).getElementsByTagName("p")[3].innerHTML;
}

function ChangeAccountInfo() {
    document.getElementById("account-save-button").style.display = "block";
    document.getElementById("account-info").style.display = "none";
    document.getElementById("account-change").style.display = "block";
    FillAccountInput('account-info');
}

function SwitchTopBar() {
    if (!logged) {
        document.getElementById("account-top-bar-logged").style.display = "none";
        document.getElementById("account-top-bar").style.display = "block";
    }
    else {
        document.getElementById("account-top-bar-logged").style.display = "block";
        document.getElementById("account-top-bar").style.display = "none";
    }
}

function SizeDownScreen(sidebar) {
    document.getElementById('main').className += " m8 l9";
    document.getElementById(sidebar).style.display = "block";
}

function SizeUpScreen() {
    document.getElementById('main').classList.remove("l9");
    document.getElementById('main').classList.remove("m8");
}
function SwitchBackScreen() {
    FillComfirmInfo();
    SwitchScreen(backScreen);
    SizeUpScreen();
}
var active;
function SwitchScreen(screenName) {
    currentScreen = screenName;
    if (currentScreen !== 'account' && currentScreen !== 'transaction') {
        backScreen = currentScreen;
    }
    var screens = document.getElementsByClassName("customer-screen");
    for (var i = 0; i < screens.length; i++) {
        screens[i].style.display = "none";
    }
    ResetInputs();
    document.getElementById(currentScreen).style.display = "block";
    if (currentScreen === 'car') {
        SizeDownScreen('car-side-bar');
        if (logged) {document.getElementById("account-top-bar-logged").style.display = "none";}
        else { document.getElementById("account-top-bar").style.display = "none"; }
        document.getElementsByClassName("mobile-menu")[0].style.display = "none";
    }
    else if (currentScreen === 'account' || currentScreen === 'transaction') {
        SizeDownScreen('setting-side-bar');
    }
    else if (currentScreen === 'date') {
        if (logged) { document.getElementById("account-top-bar-logged").style.display = "block"; }
        else { document.getElementById("account-top-bar").style.display = "block"; }
        document.getElementsByClassName("mobile-menu")[0].style.display = "block";
        SizeUpScreen();
    }
    else {
        SizeUpScreen();
    }
}

function SwitchScreenCar(screenName) {
    postCar = '';
    if (selectedCar === undefined) {
        alert("You must select a car to proceed");
    }
    else {
        var car = selectedCar;
        //console.log(car);
        for (var i = 0; i < car.cells.length - 1; i++) {
            FillData(car.cells[i].innerHTML, "confirm-car");
        }
        for (var i = 0; i < localCarArray.length; i++) {
            if (localCarArray[i]['Make'] === car.cells[0].innerHTML && localCarArray[i]['Model'] === car.cells[1].innerHTML && localCarArray[i]['BodyStyle'] === car.cells[2].innerHTML && localCarArray[i]['Colour'] === car.cells[3].innerHTML) {
                postCar = localCarArray[i]['VIN'];
            }(i)
        }
        
        SwitchScreen(screenName);
    }
}

function SwitchScreenDate(screenName) {
    postTransaction = {};
    if (selectedDateStart === undefined || selectedDateEnd === undefined) {
        alert("Please select a check in and a check out date");
    }
    else if (selectedLocation === undefined) {
        alert("Please select from one of our locations");
    }
    else {
        FillData(selectedDateStart, "confirm-date");
        FillData(selectedDateEnd, "confirm-date");
        FillData(selectedLocation, "confirm-date");
        postTransaction["startdate"] = selectedDateStart;
        postTransaction["enddate"] = selectedDateEnd;
        postTransaction["rentedfrom"] = 3;//selectedLocation;
        SwitchScreen(screenName);
    }
}

function SwitchScreenConfirm(screenName) {
    postCustomer = {};
    var f = document.getElementById("first-name").value;
    var l = document.getElementById("last-name").value;
    var p = document.getElementById("phone").value;
    var e = document.getElementById("email").value;
    if (f === '' || l === '' || p === '' || e === '') {
        alert("Please fill out all forms");
    }
    else {
        postCustomer["firstname"] = f;
        postCustomer["lastname"] = l;
        postCustomer["phonenumber"] = p;
        postCustomer["email"] = e;
        postCustomer["password"] = "password";
        if (logged) {
            PushData(postCustomer, "customers", Empty());
        }
        SwitchScreenConfirm2(screenName);
    }
}

function SwitchScreenConfirm2(screenName) {
    postTransaction["car"] = postCar;
    postTransaction["customer"] = (parseInt(largestCID) + 1).toString();
    postTransaction["amountdue"] = "100000.00";
    console.log(postTransaction);
    if (logged) {
        postTransaction["customer"] = globaluserid;
    }
    setTimeout(PushData(postTransaction, "transactions", Empty()), 1000);

    if (logged === false) {
        //document.getElementById('confirm-login').style.display = 'block'; //Offender
        //SwitchScreen('confirm-login');
        SwitchScreen('date');
    }
    else {
        //ParseNewTransaction();
        SwitchScreen(screenName);

    }
    //SwitchScreen(screenName);
}


function Empty(cb) {
    console.log(cb);
}

function ApplyCarFilter() {
    ClearTable("car-table");
    var carFilters = document.getElementsByClassName("car-filter");
    for (var i = 0; i < filters.length; i++) {
        if (carFilters[i].checked) {
            filters[i] = true;
        }
        else {
            filters[i] = false;
        }
    }
    CullTable();
}

function AddSelect(index) {
    for (var i in localCarArray) {
        if (localCarArray[i]['VIN'] === vin) {
            AddCarRow(localCarArray[i]);
        }
    }
}

function CullTable() {
    if (!filters[0] && !filters[1] && !filters[2] && !filters[3] && !filters[4] && !filters[5]) {
        PullData('cars');
    }
    for (var j = localCarArray.length - 1; j > 0; j--) {
        if (filters[0] === true && localCarArray[j]['BodyStyle'] === 'Economy') { AddCarRow(localCarArray[j]); }
        else if (filters[1] === true && localCarArray[j]['BodyStyle'] === 'Compact') { AddCarRow(localCarArray[j]); }
        else if (filters[2] === true && localCarArray[j]['BodyStyle'] === 'Midsize') { AddCarRow(localCarArray[j]); }
        else if (filters[3] === true && localCarArray[j]['BodyStyle'] === 'Minivan') { AddCarRow(localCarArray[j]); }
        else if (filters[4] === true && localCarArray[j]['BodyStyle'] === 'SUV') { AddCarRow(localCarArray[j]); }
        else if (filters[5] === true && localCarArray[j]['BodyStyle'] === 'Other') { AddCarRow(localCarArray[j]); }
    }

}

function ClickOnRow(row, table, color) {
    var oldColor = "white";
    ClearSelected(table, oldColor);
    selectedCar = row;
    row.style.background = color;
}

function ClearGlobal()  {
    selectedCar = undefined;
    selectedDateStart = undefined;
    selectedDateEnd = undefined;
    filters = [false, false, false, false, false, false];
}

function ClearSelected(table, color) {
    t = document.getElementById(table);
    for (var i = 1; i < t.rows.length; i++) {
        t.rows[i].style.background = color;
    }
}

function ClearTable(tableName) {
    var table = document.getElementById(tableName);
	
    for (var i = table.rows.length-1; i > 0; i--) {
        table.deleteRow(i);
    }(i)
}

function ResetInputs() {
    var elements = document.getElementsByTagName("form");
    for (var i = 0; i < elements.length; i++) {
        elements[i].reset();
    }
}

function SetStartDate() {
    selectedDateStart = ParseDate(document.getElementById("date-start").value);
}

function SetEndDate() {
    selectedDateEnd = ParseDate(document.getElementById("date-end").value);
}

function SetLocation() {
    selectedLocation = document.getElementById("choose-branch").value;
}
//input YYYY-MM-DD; expects trailing 0s
//output DD/MM/YYYY
function ParseDate(date) {
    var spl = date.split("-");
    return parseInt(spl[2]).toString() + "/" + parseInt(spl[1]).toString() + "/" + parseInt(spl[0]).toString();
}

function FillData(data, destination) {
    var para = document.createElement("p");
    var node = document.createTextNode(data);
    if (data === "") {
        return;
    }
    para.appendChild(node);
    var element = document.getElementById(destination);
    element.appendChild(para);
}

function RemoveData(prnt) {
	var node = document.getElementById(prnt);
	//console.log(node.childElementCount);
	while (node.hasChildNodes()){
	    node.removeChild(node.lastChild);
	}
}
function PullCustomersInfo (email) {
	var data = new FormData();
	data.append('action', 'customers');
        data.append('email', email);
        outPost(data, "http://4dudes.servebeer.com/backend/handler.py", FillCustomerInfo);
}
function FillCustomerInfo (string) {
	var obj = JSON.parse(string);
	for (var i in obj) {
		AddCustomerInfo(obj[i]);
	}
}
function AddCustomerInfo (data) {
	var values = ['FirstName', 'LastName','Password', 'PhoneNumber'];
	for (var i in values) {
            if (data['Email'] === globalemail) {
                FillData(data[values[i]],"account-info");
                globaluserid = data['CID'];
            }
	}
}

function PullTransactionInfo () {
        ClearTable("trasaction-history-table");
	var data = new FormData();
	data.append('action', 'transactions');
        
	outPost(data, "http://4dudes.servebeer.com/backend/handler.py", FillTransactionInfo);
}
function FillTransactionInfo (string) {
	var obj = JSON.parse(string);
	for (var i in obj) {
		AddTransactionInfo(obj[i]);
	}
        
}
function AddTransactionInfo(data) {
    var table = document.getElementById("trasaction-history-table");
    var row = table.insertRow(1);
    row.onclick = function () { ClickOnRow(row, table, "#eeffe0"); };
    row.onmousemove += row.style.cursor = "pointer";
    row.style.background="white";
    var values = ['Customer', 'Car', 'StartDate','EndDate', 'AmountDue'];
    var IsAddElement = false;
    for (var i in values) {
        if (data['Customer'] === globaluserid) {
            var cell = row.insertCell();
            cell.innerHTML = data[values[i]];
            IsAddElement = true;
            if (i === 4) {
            cell.innerHTML += "20% off for GodlmemberShip";
            }
        }
        
    }
    
    if (IsAddElement === false) {
        document.getElementById("trasaction-history-table").deleteRow(1);
    }
    var table = document.getElementById("trasaction-history-table");
    if (table.rows.length > 3) {
        document.getElementById("top-bar-right").style.display = "block";
    }
    
}

function ResetTable(table) {
    for (var i = table.rows.length-1; i > 0; i--) {
        table.deleteRow(i);
    }
}

function AddCustomerRow (data) {
	var table = document.getElementById("transaction-history-table");
	var row = table.insertRow(1);
        row.onmousemove = row.style.cursor="pointer";
	row.style.background="white";
	var values = ['Customer', 'Car', 'StartDate','EndDate', 'AmountDue'];
	for (var i in values) {
            var cell = row.insertCell();
            cell.innerHTML = data[values[i]];
            if (values[i] === 'Customer') {
                username = cell.innerHTML;
                PullCustomersData();
            }
	} 
}

function AddCustomerRow (data) {
	var values = ['FirstName', 'LastName','CID'];      
	if (data['CID'] === Number(username)) {
            username = "";
            username += data['FirstName'] + " ";
            username += data['LastName'];
            var table = document.getElementById("trasaction-history-table");
            table.rows[1].cells[0].innerHTML = username;
        }
}

function FillCustomerData (string) {
    var obj = JSON.parse(string);
    for (var i in obj) {
        AddCustomerRow(obj[i]);
    }
}

function PullData(remote) {
    var data = new FormData();
    data.append('action', remote);
    switch (remote) {
        case 'cars':
            outPost(data, "http://4dudes.servebeer.com/backend/handler.py", FillCarsData);
            break;
        default:
            break;
    }
}

function GetCID() {
    var data = new FormData();
    data.append('action', 'customers');
    outPost(data, "http://4dudes.servebeer.com/backend/handler.py", FindLargestCID);
}

function FindLargestCID(string) {
    var obj = JSON.parse(string);
    for (var i in obj) {
        if (parseInt(obj[i]['CID']) > largestCID) { largestCID = parseInt(obj[i]['CID']);}
    }
    console.log(largestCID);
}

function PushData(postData, remote, cb) {
    var post = new FormData();
    var url;
    post.append('action', remote);
    for (var i in postData) {
        post.append(i, postData[i]);
    }

    outPost(post, "http://4dudes.servebeer.com/backend/addhandler.py", cb);
    
}

function FillCarsData(string) {
    var obj = JSON.parse(string);
    for (var i in obj) {
        if (AddCarsRow(obj[i]) >= 0) {
            localCarArray.push(localCarCopy);
            localCarCopy = {};
        }
    }
    initialLoad = false;
}
function AddCarsRow(data) {
    if (data['AtBranch'] !== globalbranch) {
        return -1;
    }
    AddCarRow(data);
    return 0;
}
function AddCarRow(data) {
    var table = document.getElementById("car-table");
    var row = table.insertRow(1);
    row.onclick = function () { ClickOnRow(row, "car-table", "#eeffe0"); };
    row.onmousemove += row.style.cursor = "pointer";
    var values = ['Make', 'Model', 'BodyStyle', 'Colour', 'Class', ''];
    for (var i in values) {
        var cell = row.insertCell();
        cell.innerHTML = data[values[i]];
        if (initialLoad) { 
            localCarCopy[values[i]] = data[values[i]];
            localCarCopy['VIN'] = data['VIN'];
        }
    }
}


function SwitchMobile() {
    var x = document.getElementsByClassName("mobile-top-bar");
    //console.log(x[0], x[1], logged);
    if (!logged) {
        if (x[0].className.indexOf("w3-show") == -1) {
            x[0].className += " w3-show";
        } else {
            x[0].className = x[0].className.replace(" w3-show", "");
        }
    }
    else {
        if (x[1].className.indexOf("w3-show") == -1) {
            x[1].className += " w3-show";
        } else {
            x[1].className = x[1].className.replace(" w3-show", "");
        }
    }
}

function Test() {
    var data = new FormData();
    data.append("action", "cars");
    cb = function (cbt) {
        obj = JSON.parse(cbt);
        console.log(cbt);
    }
    outPost(data, "http://4dudes.servebeer.com/backend/handler.py", cb);
}

window.onclick = function (event) {
    var modals = document.getElementsByClassName('w3-modal');
    for (var i = 0; i < modals.length; i++) {
        var modal = modals[i]
        if (event.target == modal) {
            modal.style.display = "none";
            ResetInputs();
        }(i)
    }
}

function onload() {
    //PullData('cars');
    GetCID();
}

document.onload = onload();